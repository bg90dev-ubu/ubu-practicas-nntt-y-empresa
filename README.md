<div align="center">

# Nuevas Tecnologías y Empresa

[![Project](https://img.shields.io/badge/Project-University-blueviolet.svg)][repo-link]
[![Repository](https://img.shields.io/badge/gitlab-purple?logo=gitlab)][repo-link]
[![Language](https://img.shields.io/badge/Python-4B8BBE?logo=python&logoColor=FFE873)][python-link]
[![Original-Repository](https://img.shields.io/badge/bitbucket-Original_Repository-0747A6?logo=bitbucket)][original-repo-link]

Prácticas de la asignatura de Nuevas Tecnologías y Empresa de 4º curso
del grado de Ingeniería Informática de la Universidad de Burgos.

</div>
<hr>

## Built with

### Technologies

[<img src="https://raw.githubusercontent.com/BorjaG90/media/master/img/logos/python.png" width=50 alt="Python">][python-link]

### Platforms

[<img src="https://raw.githubusercontent.com/BorjaG90/media/master/img/logos/jupyter.png" width=50 alt="Jupyter Notebook">][jupyter-link]


<div align="center">

## Authors

### **Borja Gete**

[![Mail](https://img.shields.io/badge/borjag90dev@gmail.com-DDDDDD?style=for-the-badge&logo=gmail)][borjag90dev-gmail]
[![Github](https://img.shields.io/badge/BorjaG90-000000.svg?&style=for-the-badge&logo=github&logoColor=white)][borjag90dev-github]
[![Gitlab](https://img.shields.io/badge/BorjaG90-purple.svg?&style=for-the-badge&logo=gitlab)][borjag90dev-gitlab]
[![LinkedIn](https://img.shields.io/badge/borjag90-0077B5.svg?&style=for-the-badge&logo=linkedin&logoColor=white)][borjag90dev-linkedin]

### **Fernán Ramos**

[![Mail](https://img.shields.io/badge/ferburgos93@gmail.com-DDDDDD?style=for-the-badge&logo=gmail)][fernan-gmail]
[![Github](https://img.shields.io/badge/Fernan_Ramos-000000.svg?&style=for-the-badge&logo=github)][fernan-github]
[![LinkedIn](https://img.shields.io/badge/Fernan_Ramos-0077B5.svg?&style=for-the-badge&logo=linkedin&logoColor=white)][fernan-linkedin]


</div>

[borjag90dev-gmail]: mailto:borjag90dev@gmail.com
[borjag90dev-github]: https://github.com/BorjaG90
[borjag90dev-gitlab]: https://gitlab.com/BorjaG90
[borjag90dev-linkedin]: https://www.linkedin.com/in/borjag90/
[fernan-gmail]: mailto:ferburgos93@gmail.com
[fernan-linkedin]: https://www.linkedin.com/in/fern%C3%A1n-ramos-saiz-337807147/
[fernan-github]: https://github.com/Fernan-Ramos
[original-repo-link]: https://BorjaG90@bitbucket.org/BorjaG90/practicas_nnttyempresa.git
[repo-link]: https://gitlab.com/bg90dev-ubu/ubu-practicas-nntt-y-empresa
[python-link]: https://www.python.org/doc/
[jupyter-link]: https://jupyter.org/documentation
